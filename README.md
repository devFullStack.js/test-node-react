# Prérequis
  Docker / Node (v20.11.0) / mongodb avec pour 27017

# Test local
  back end 
    - cd repertoir de project
    - npm i
    - npm run dev
    - url: http://localhost:4000/api-docs/#/
    
  Back office
    - cd bo
    - npm i --force
    - npm start
    - url: http://localhost:8008/auth

# Build
  docker-compose up -d --build

# back
  swagger url: http://localhost:4000/api-docs/#/

# BO
  front-url: http://localhost:4000/app
  compte test: testapps@gmail.com / TestApp#123

# NB
  ormconfig.js est le fichier de conficuration pour cumminiqué entre le Back end et le Base de donné mongodb
  Si en local le ormconfig.js
    module.exports = {
      "type": "mongodb",
      <!-- "host": "mongo", -->
      "host": "localhost",
      "port": 27017,
      "database": "docker-node-mongo",
      "useUnifiedTopology": true,
      "useNewUrlParser": true,
      "synchronize": false,
      "entities": [
        <!-- "dist/data/do/**/*.js", -->
        "src/data/do/**/*.ts"
      ],
      "seeds": [
        <!-- "dist/seeds/*.js", -->
        "src/seeds/*.ts"
      ],
    }

  Et Si en prod dans docker le ormconfig.js
     module.exports = {
      "type": "mongodb",
      "host": "mongo",
      <!-- "host": "localhost", -->
      "port": 27017,
      "database": "docker-node-mongo",
      "useUnifiedTopology": true,
      "useNewUrlParser": true,
      "synchronize": false,
      "entities": [
        "dist/data/do/**/*.js",
        <!-- "src/data/do/**/*.ts" -->
      ],
      "seeds": [
        "dist/seeds/*.js",
        <!-- "src/seeds/*.ts" -->
      ],
    }