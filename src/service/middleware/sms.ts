import axios from 'axios';

export const sendSMS = async (messages: string, receiver: string) => {
  try {
    const res = await axios.post(
      process.env.SMS_URL,
      {
        messages,
        sender: process.env.SENDER,
        receiver,
      },
      {
        headers: {
          application: 'TestApps',
          'Content-Type': 'application/json',
        },
      },
    );
    return res.data;
  } catch (error) {
    return error;
  }
};
