import { GenericSA } from '../../common/service/generic.sa';
import {
    productFactory,
  ProductFactory,
} from '../../constraint/factory/Product';
import { ProductDO } from '../../data/do/Product';
// @ts-ignore
import { ProductRequestDTO } from '../../data/dto/Product/request';
// @ts-ignore
import { ProductResponseDTO } from '../../data/dto/Product/response';
import { productSM, ProductSM } from '../metier/Product';

export class ProductSA extends GenericSA<
  ProductDO,
  ProductRequestDTO,
  ProductResponseDTO,
  ProductSM,
  ProductFactory
> {
}

export const productSA = new ProductSA(productSM, productFactory, 'Product');

