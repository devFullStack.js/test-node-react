import { getCustomRepository } from 'typeorm';

import { GenericSM } from '../../common/service/generic.sm';
import { ProductDO } from '../../data/do/Product';
import { ProductRepository } from '../../repository/Product';

export class ProductSM extends GenericSM<ProductDO, string, ProductRepository> {
}

export const productSM = new ProductSM(getCustomRepository(ProductRepository));

