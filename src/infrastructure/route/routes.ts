import { Router } from 'express';
import { conditionnalJwtPassport } from '../../service/middleware/passport/conditionnal-jwt-passport';
import { productRouter } from './Product';
import { utilisateurRouter } from './Utilisateur';

export const routes = () => {
  const router = Router();
  const secured = conditionnalJwtPassport(true);
 router.use('/product', productRouter);
 router.use('/utilisateur', utilisateurRouter);
  
  return router;
};

