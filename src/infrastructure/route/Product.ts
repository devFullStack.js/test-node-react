import { genericRoute } from '../../common/infrastructure/generic.route';
import Joi from '../../constraint/validator/joi';
import { productController } from '../controller/Product';

const productRoutes = () => genericRoute({ controller: productController, schema: Joi.Product, name: 'Product' });

export const productRouter = productRoutes();

/**
 * GET /api/product
 * @tags Product
 * @security BearerAuth
 * @summary List Product (getAllProduct)
 
 * @param {number} page.query
 * @param {number} rowPerPage.query
 * @param {string} data.query
 * @param {string} TemplateParameter1.path.required 
 * @return {ProductResponseDTO} 201
 * @return {object} 400 - Données non conformes
 * @return {object} 500 - Erreur interne du serveur
 */

/**
 * DELETE /api/product
 * @tags Product
 * @security BearerAuth
 * @summary Remove Product (deleteProduct)
  
 * @return {DeleteResponseDTO} 200
 * @return {object} 400 - Données non conformes
 * @return {object} 500 - Erreur interne du serveur
 */

/**
 * PUT /api/product/partialUpdate/
 * @tags Product
 * @security BearerAuth
 * @summary Update Product (updateProduct)
 * @param {ProductRequestDTO} request.body 
 * @return {UpdateResponseDTO} 200
 * @return {object} 400 - Données non conformes
 * @return {object} 500 - Erreur interne du serveur
 */

/**
 * GET /api/product
 * @tags Product
 * @security BearerAuth
 * @summary List Product (getProduct)
  
 * @return {ProductResponseDTO} 201
 * @return {object} 400 - Données non conformes
 * @return {object} 500 - Erreur interne du serveur
 */

/**
 * GET /api/product/count/elements
 * @tags Product
 * @security BearerAuth
 * @summary List Product (countProduct)
  
 * @return {CountResponseDTO} 200
 * @return {object} 400 - Données non conformes
 * @return {object} 500 - Erreur interne du serveur
 */

