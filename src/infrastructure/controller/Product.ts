import { GenericController } from '../../common/infrastructure/generic.controller';
import { ProductDO } from '../../data/do/Product';
// @ts-ignore
import { ProductRequestDTO } from '../../data/dto/Product/request';
// @ts-ignore
import { ProductResponseDTO } from '../../data/dto/Product/response';
import { productSA, ProductSA } from '../../service/applicatif/Product';

class ProductController extends GenericController<
  ProductDO,
  ProductRequestDTO,
  ProductResponseDTO,
  ProductSA
> {}

export const productController = new ProductController(productSA);

