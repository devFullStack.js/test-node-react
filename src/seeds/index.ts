import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';
import * as bcrypt from 'bcryptjs';
import { UtilisateurRepository } from '../repository/Utilisateur';
import { ProductRepository } from '../repository/Product';

export class InitSeeds implements Seeder {
  // eslint-disable-next-line class-methods-use-this
  async run(_factory: Factory, connection: Connection): Promise<void> {
    const user = connection.getCustomRepository(UtilisateurRepository);
    const product = connection.getCustomRepository(ProductRepository);

    await product.createCollectionIndex({ name: 'text', description: 'text' });
    await user.createCollectionIndex({
      nom: 'text',
      prenom: 'text',
      email: 'text',
      adresse: 'text',
    });

    const users = await user.count();
    if (users < 1) {
      try {
        const data = [
          {
            password: await bcrypt.hashSync('TestApp#123', 10),
            nom: 'Name',
            prenom: 'Test',
            email: 'testapps@gmail.com',
            ville: '',
            adresse: '',
            imageUrl: '',
            role: 1,
            actif: true,
            createdAt: new Date(),
          },
        ];
        await user.save(data);
      } catch (error) {
        console.log('error ====================================');
        console.log(error);
        console.log('====================================');
      }
    }
  }
}
