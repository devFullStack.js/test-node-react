import { EntityRepository, MongoRepository } from 'typeorm';

import { ProductDO } from '../data/do/Product';

@EntityRepository(ProductDO)
export class ProductRepository extends MongoRepository<ProductDO> {}

