export type PhoneResponseDTO = {
   paysCode?: string;
   phoneNumber?: string;
   callingCode?: string;
  createdAt: Date;
  updatedAt: Date;
  id: string;
}

/**
 * @typedef {object} PhoneResponseDTO
 * @property {string} paysCode
 * @property {string} phoneNumber
 * @property {string} callingCode
 * @property {string} id
 * @property {string} updatedAt
 * @property {string} createdAt
 */

