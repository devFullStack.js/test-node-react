export type ProductRequestDTO = {
   name: string;
   description: string;
   price: number;
   photo?: string;
}

/**
 * @typedef {object} ProductRequestDTO
 * @property {string} name
 * @property {string} description
 * @property {number} price
 * @property {string} photo
 */

