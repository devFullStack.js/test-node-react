export type ProductResponseDTO = {
   name: string;
   description: string;
   price: number;
   photo?: string;
  createdAt: Date;
  updatedAt: Date;
  id: string;
}

/**
 * @typedef {object} ProductResponseDTO
 * @property {string} name
 * @property {string} description
 * @property {number} price
 * @property {string} photo
 * @property {string} id
 * @property {string} updatedAt
 * @property {string} createdAt
 */

