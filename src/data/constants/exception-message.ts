export const exceptionMessage = {
  OVER_MAX_SELECTED_ANNONCE:
    'Le nombre de démarche et de produit mise en avant ne doit être supérieur à 5',
  EXISTING_ORDER: `L'ordre fourni existe déjà`,
  IMAGE_NOT_PROVIDED: 'Image requise',
  DEMARCHE: 'une démarche',
};

export const exceptionCode = {
  DELETE_UPDATE_FK_ERROR: '23503',
};
