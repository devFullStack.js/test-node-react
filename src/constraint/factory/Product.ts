import { GenericFactory } from '../../common/constraint/factory/generic.factory';
import { formatDateToLocaleWithHour } from '../../common/service/date.service';
import { dataTDO } from '../../data';
import { ProductDO } from '../../data/do/Product';
// @ts-ignore
import { ProductRequestDTO } from '../../data/dto/Product/request';
// @ts-ignore
import { ProductResponseDTO } from '../../data/dto/Product/response';

const commonSchema = {
  ...dataTDO.Product.attributes.reduce((acc, { key }) => ({ ...acc, [key]: key }), {}),
};
const schema = { ...commonSchema };
const responseSchema = {
  ...commonSchema,
  id: '_id',
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
};

export class ProductFactory extends GenericFactory<
  ProductDO,
  ProductRequestDTO,
  ProductResponseDTO
> {}

export const productFactory = new ProductFactory(schema, schema, responseSchema);

