export const authentification = {
  baseApi: '/api/authentification',
  verifyResetToken: '/api/authentification/verify-reset-token',
  resetPassword: '/api/authentification/reset-password',
};
export const utilisateur = {
  baseApi: '/api/utilisateur',
  inscription: '/api/inscription',
};

export const product = {
  baseApi: '/api/product',
};

export const notification = {
  baseApi: '/api/notification',
};
