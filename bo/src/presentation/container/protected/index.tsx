import * as React from 'react';

import applicatif from "../../../service/applicatif";
import { dataTDO } from "../../../data";
import { EntityContainer } from "../../../common/protected";

export default Object.keys(dataTDO).reduce((acc, entity) => {
  const { route, attributes } = dataTDO[entity];

  return {
    ...acc,
    [entity]: () => {
      const getInitialValue = async () => {
        let res = {};
        try {
          if (Array.isArray(attributes)) {
            const selects = attributes.filter((attribute) => attribute?.entity?.method);
            if (selects.length > 0) {
              const pResect = await Promise.all(
                selects.map(async ({ entity, name }) => {
                  const { data } = await applicatif[entity.name]().getAll({page: 1, rowPerPage:10});
                  if (data?.items) {
                    return { value: data?.items, key: name };
                  }
                  return { key: name, value: [] };
                }),
              );
              res = pResect.reduce((acc, curr) => ({ ...acc, [curr?.key]: curr?.value }), res);
            }
          }
          return res;
        } catch (error) {
          console.log({ error });
          return {};
        }
      };
      return (
        <EntityContainer
          properties={attributes}
          useEntitySA={applicatif[entity]}
          entityUrl={`/api/${route}`}
          defaultSort={{ id: 'name', desc: false }}
          getInitialValue={getInitialValue}
          relation={false}
          currentEntity={entity}
        />
      );
    },
  };
}, {});
