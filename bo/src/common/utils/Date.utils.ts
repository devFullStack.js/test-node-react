import * as dayjs from 'dayjs';

require('dayjs/locale/fr');

const format = 'YYYY-MM-DDTHH:mm:ss.sssZ';

export const formatDateToLocaleWithHour = (dateHour) =>
  dayjs(dateHour, format).format('DD/MM/YYYY, à HH[h]mm');

export const formatDateToLocale = (date) =>
  date ? dayjs(date, format).format('DD/MM/YYYY') : dayjs().format('DD/MM/YYYY');
