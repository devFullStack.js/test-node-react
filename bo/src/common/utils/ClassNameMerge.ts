export const mergeClassNames = (baseClassNames: string, customClassName: string) =>
  `${baseClassNames
    .split(' ')
    .filter((classMember) => !customClassName.includes(classMember.split('-')[0]))
    .join(' ')} ${customClassName}`;
