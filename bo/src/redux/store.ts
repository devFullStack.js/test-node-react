import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { combinedReducer } from './reducers';

const store = createStore(combinedReducer, composeWithDevTools(applyMiddleware(ReduxThunk)));

export { store };
