import { combineReducers } from 'redux';

import { authReducer as auth, AuthState } from './auth';
import { appReducer as app } from './app';

export type RootState = {
  auth: AuthState;
};

export const combinedReducer = combineReducers({
  auth,
  app,
});
